<!DOCTYPE html>
<html lang="en">
<?php require('head.php'); ?>
<?php require('menu.php'); ?>

<div class="col-md-12">
    <div class="col-sm-3">
	     <?php require_once('./info.php'); ?>
    </div>

	  <div class="col-sm-9"> <!--body droit-->
	     <?php require_once('./presentation.php'); ?>
  	</div>

	  <?php require('footer.php'); ?>
</div> <!-- end col 12-->

</html>
