<!DOCTYPE html>
<?php
if($_POST['lang']='eng')
{
  //echo anglais
}
else
{
  //echo francais
}
?>

<html lang="en">
<?php require('head.php'); ?>
<body>
<?php require('menu.php'); ?>

<div class="col-md-12">
    <div class="col-sm-3">
	<?php require_once('./info.php'); ?>
    </div>

	<div class="col-sm-9"> <!--body droit-->
        <br><br>
        <h3><center>Mes Projets</center></h3>
	<?php require_once('./projets.php'); ?>        
	</div>

	</div> <!-- end body droit -->
	<?php require('footer.php'); ?>
</div> <!-- end col 12-->

</body>
</html>
