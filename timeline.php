<div class="container">
<link rel="stylesheet" href="./css/timeline.css">
    <div class="page-header">
        <h1 id="timeline">
            <div class="container text-center">Expériences Professionelles</h1></div>
    </div>
    <ul class="timeline">
        <li class="timeline-inverted">
          <div class="timeline-badge warning"><i class="glyphicon glyphicon-book"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Séries de Projets</h4>
                <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 2015 - 2017</small></p>
            </div>
            <div class="timeline-body">
                <p><b>Tous différents, vous pouvez les consulter sur 'Portfolio'</b></p>
                  <p>2048 - Language C</p>
                <p>St Denis Dream Team - HTML/CSS</p>
                <p>MMORPG - Java</p>
                <p>AgriWork - Php/Bootstrap/MySQL</p>
            </div>
          </div>
        </li>        
        <li>
          <div class="timeline-badge"><i class="glyphicon glyphicon-book"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Début de Formation IUT</h4>
              <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 2015 - 2017</small></p>
            </div>
            <div class="timeline-body">
                <p><b>Au sein de l'université Paris XIII (93)</b></p>
                  <p>Détails sur la formation</p>
            </div>
          </div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-badge warning"><i class="glyphicon glyphicon-piggy-bank"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Assistant vendeur - Job Etudiant</h4>
                <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Décembre 2015 - Avril 2016</small></p>
            </div>
            <div class="timeline-body">
              <p><b>Cap Fraicheur, Herblay</b></p>
              <p>Vente clients et remise en rayon</p>
            </div>
          </div>
        </li>
        <li>
          <div class="timeline-badge danger"><i class="glyphicon glyphicon-piggy-bank"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Assistant Cuisine - Job d'été</h4>
                <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Juin 2014 - Aout 2014</small></p>
            </div>
            <div class="timeline-body">
              <p><b>Grill Saint Martin, Verneuil /s Avre</b></p>
                <p>Plonge et responsabilités des entrés du restaurant </p>
            </div>
          </div>
        </li>
        <li class="timeline-inverted">
         <div class="timeline-badge warning"><i class="glyphicon glyphicon-book"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Classe Prépa INSA</h4>
                <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Septembre 2013 - Juin 2014</small></p>
            </div>
            <div class="timeline-body">
                <p><b>Première année à l'insa Bourges, école d'ingénieur région Centre</b></p>
              <p>Mathématiques, sciences appliqués</p>
            </div>
          </div>
        </li>
        <li>
          <div class="timeline-badge info"><i class="glyphicon glyphicon-piggy-bank"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Manutention - Job d'été</h4>
                <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Juillet 2013</small></p>
            </div>
            <div class="timeline-body">
              <p><b>Sabatier, Vénissieux</b></p>
              <p>Préparation des envois</p>
            </div>
          </div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-badge success"><i class="glyphicon glyphicon-book"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">Baccalauréat Scientifique</h4>
              <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Juin 2013</small></p>
            </div>
            <div class="timeline-body">
                <p><b>Lycée Porte de Normandie</b></p>
                <p> Mention bien, spécialité Informatique et Science du numérique</p>
            </div>
          </div>
        </li>
    </ul>
</div>
