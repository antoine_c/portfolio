
<script>
(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });
}
</script>
    <script>

        var searchVisible = 0;
        var transparent = true;
        var transparentDemo = true;
        var fixedTop = false;
        var big_image;
        var responsive;

        var moving_line;
        var front_image;
        var back_image;

        var html_code_section;
        var sectionLayers_top;

        var image;
        var oVal = 0;
        var delay = 1;

        var params_url = '';

        $().ready(function(){
            responsive = $(window).width();
            var coupon = getUrlParameter('coupon');
            var ref = getUrlParameter('ref');
            var deal = getUrlParameter('deal');
            var deal_cookie = getCookie('deal');

            var deal_link = "https://www.inkydeals.com/deal/get-shit-done-kit-pro-premium-bootstrap-ui-kit/";

            has_param = 0;

            $('[rel="tooltip"]').tooltip();

            if(coupon){
                addQSParm("coupon", coupon);
            }
            if(ref){
                addQSParm("ref", ref);
            }

            if(deal && !deal_cookie){
                createCookie('deal','inky-deals',7);
            } else if (deal_cookie){
                deal = deal_cookie;
            }

            if(deal){
                addQSParm("deal",deal);
                $('#regularPricing').hide();
                $('.deal').removeClass('hide');
            }

            if(coupon || ref || deal){
                $('.card-info .btn').attr('href','http://www.creative-tim.com/buy/get-shit-done-pro' + params_url);
                $('#buyButton').attr('onclick','window.location.href="http://www.creative-tim.com/buy/get-shit-done-pro' + params_url + '"');
            }

            if(coupon){
                if(coupon == '50tim'){
                    $('#buyButton').html('Buy now with 50% Discount');
                } else {
                    $('#buyButton').html('Buy now with 25% Discount');
                }
            }
            if(ref || coupon || deal){
                tim_link = $("#timLink").attr('href');
                free_kit_link = $('#freeKitLink').attr('href');

                $("#timLink").attr('href', tim_link + params_url);

                $('.navbar-right a, #examples-trigger a, .dropdown-with-icons a').each(function(){
                    href = $(this).attr('href');
                    if(href != '#'){
                        $(this).attr('href',href + params_url);
                    }
                });
            }

            if(deal){
                $('#buyButton').attr('onclick','window.location.href="' + deal_link + '"').removeClass('btn-warning').addClass('btn-danger').html('Get 80% Discount Deal');
                $('#dealButton').attr('href',deal_link);
            }

            if (responsive < 768){
                initRightMenu();
            //    images.insertBefore(content);
            }else {
                big_image = $('.parallax-image').find('img');
            }

            moving_line = $('.card-html-code .line');
            front_image = $('.card-html-code .front-image');
            back_image = $('.card-html-code .back-image');

            html_code_section = $('#htmlCodeSection').offset().top - 100;

            sectionLayers_top = $('#sectionLayers').offset().top;

        });

        $(window).on('scroll',function(){
            checkScrollForPresentation();
        });

        function showModal(button){
           background = document.getElementById("background");
           $(background).addClass('loader-background');

           $('.loader').addClass('visible');

           modal = $(button).data('target');


           var image=$(modal).find('img');
           test =image.data('init');

           if(!test){
                src = image.data('src');
                image.attr('src',src);
                test=0;
                image.attr('data-init',1);
           }


        setTimeout(function(){
            $(modal).modal('show')
        },2000);


        }

        $('.modal').on('hide.bs.modal', function (e) {
             $('.loader').removeClass('visible');
              $(background).removeClass('loader-background');
        });

        var loaded_images = false;

        var checkScrollForPresentation = function() {
            var current_scroll = $(this).scrollTop();

            // stop the image parallax after the visible area
            if(current_scroll < 500 && responsive >= 768){
                oVal = ($(window).scrollTop() / 3);
                big_image.css('top',oVal);
            }

            trigger = $("#screen-trigger");
            src = $(trigger).find('img').data('src');


            if(current_scroll > 100) {
                if(!loaded_images){
                    $(trigger).find('img').attr('src',src);
                    $('.img-loader').each(function (){

                        var $image = $(this);
                        var data = $image.data('src');

                        setTimeout(function(){
                            $image.attr("src",data);
                           /*  console.log(data); */
                        }, delay * 500);
                        delay++;
                    });
                    loaded_images = true;
                }
            }


            if ($("#navbar-accueil")[0]){
                if(current_scroll > 70 ) {
                    if(transparent) {
                        transparent = false;
                        $('nav[role="navigation"]').removeClass('navbar-transparent').addClass('navbar-small');
    /*                     $('#buyButton').addClass('btn-warning').removeClass('btn-neutral'); */

                    }
                } else {
                    if( !transparent ) {
                        transparent = true;
                        $('nav[role="navigation"]').addClass('navbar-transparent').removeClass('navbar-small');
    /*                     $('#buyButton').addClass('btn-neutral').removeClass('btn-warning'); */
                        $('.logo-container').removeClass('small');
                    }
                }
            }


            top_original = sectionLayers_top - 200;

                if (current_scroll > top_original){
                    $('#layerImage').addClass('animate down');
                    $('#layerBody').addClass('animate down-2x');
                    $('.layers-container').addClass('expanded');
                } else {
                    $('#layerImage').removeClass('down');
                    $('#layerBody').removeClass('down-2x');
                    $('.layers-container').removeClass('expanded');
                }


            move_line_distance = (current_scroll - html_code_section) * 1.3;
            move_line_distance = Math.ceil(move_line_distance);

            if (current_scroll > html_code_section){
                if(move_line_distance <= 460){
                    moving_line.css('left',move_line_distance);
                    front_image.css('width', 460 - move_line_distance);
                    back_image.css('width', move_line_distance);
                }

            }

        };


    var navbar_menu_visible= 0;

    function initRightMenu(){
         $('[data-nav-image]').each(function(){
                var image = $(this).data('nav-image');
                $(this).css('background',"url('" + image + "')").removeAttr('data-image');
                    $(this).css('background-size',"cover");

            });

         $('.navbar-collapse').css('min-height', window.screen.height);
         navbar = $('nav').find('.navbar-collapse');
         $('body').append(navbar);

         $toggle = $('.navbar-toggle');

         $(navbar).find('a').removeClass('btn btn-round btn-default');
         $(navbar).find('button').removeClass('btn-round btn-fill btn-info btn-primary btn-success btn-danger btn-warning btn-neutral');
         $(navbar).find('button').addClass('btn-simple btn-block');

         $toggle.click(function (){
            if(navbar_menu_visible == 1) {
                $('html').removeClass('nav-open');
               navbar_menu_visible = 0;
                $('#bodyClick').remove();
                 setTimeout(function(){
                    $toggle.removeClass('toggled');
                 }, 400);

            } else {
                setTimeout(function(){
                    $toggle.addClass('toggled');
                }, 430);

                div = '<div id="bodyClick"></div>';
                $(div).appendTo("body").click(function() {
                    $('html').removeClass('nav-open');
                    navbar_menu_visible = 0;
                    $('#bodyClick').remove();
                     setTimeout(function(){
                        $toggle.removeClass('toggled');
                     }, 400);
                });

                $('html').addClass('nav-open');
                navbar_menu_visible = 1;

            }
        });
    }

    function getUrlParameter(sParam){
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam){
                return sParameterName[1];
            }
        }
    }



    function addQSParm(name, value) {
        var re = new RegExp("([?&]" + name + "=)[^&]+", "");

        function add(sep) {
            params_url += sep + name + "=" + encodeURIComponent(value);
        }

        function change() {
            params_url = params_url.replace(re, "$1" + encodeURIComponent(value));
        }
        if (params_url.indexOf("?") === -1) {
            add("?");
        } else {
            if (re.test(params_url)) {
                change();
            } else {
                add("&");
            }
        }
    }

    var createCookie = function(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function getCookie(c_name) {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) {
                    c_end = document.cookie.length;
                }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }

      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-46172202-1', 'auto');
      ga('send', 'pageview');


    </script>

<script type="text/javascript">
  (function() {
    window._pa = window._pa || {};
    // _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions
    // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
    // _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.marinsm.com/serve/56026dc590a7b5bf54000030.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
  })();
</script>