﻿<!DOCTYPE html>
<?php
if($_POST['lang']='eng')
{
  //echo anglais
}
else
{
  //echo francais
}
?>

<html lang="en">
<?php require('head.php'); ?>
<body>
<?php require('menu.php'); ?>

<div class="col-md-12">
	<div class="col-sm-3 sidenav">
		<div class="jumbotron">
			<address>
	 		 <strong>Developpeur</strong><br>
	  		95130 Franconville<br>
	  		<abbr title="Phone">Telephone:</abbr> (+33) 9 86 15 65 03
			</address>

			<address>
	 		 <strong>Antoine Cervo</strong><br>
	 		 <a href="mailto:#">antoine.cervo@gmail.com</a><br>
			Etudiant à l'Université Paris 13<br>
			IUT Villetaneuse Informatique
			</address>
			  Vous pouvez consulter sur ce site : <br><br>
			<dl><ul>
			  <dt><li>Mon CV détaillé</li></dt>
			    <dd>Téléchargeable ici en pdf</dd>
			  <dt><li>Mon cursus</li></dt>
			    <dd>De mon bac à aujourd'hui</dd>
			  <dt><li>Mes exp. professionnelles</li></dt>
			    <dd>Majoritairement des jobs d'été</dd>
		  	  <dt><li>Mes projets d'études</li></dt>	
			    <dd>Objet, Bdd, Web</dd>	  
			  <dt><li>Mes logiciels utiles</li></dt>
			    <dd>IDE, Editeurs, UML</dd>
			  <dt><li>Un moyen de contact</li></dt>		 
			    <dd>@mail & Téléphone</dd> 
			  </ul>
			</dl>
		</div>
	</div>

	<div class="col-sm-9"> <!--body droit-->
	<?php require_once('./timeline.php'); ?>
	</div>

	</div> <!-- end body droit -->
	<?php require('footer.php'); ?>
</div> <!-- end col 12-->

</body>
</html>
