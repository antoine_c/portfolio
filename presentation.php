<link rel="stylesheet" href="./css/card.css">
<link rel="stylesheet" href="./css/progress_bar.css">
<div class="col-lg-12 col-sm-12">
    <div class="card hovercard">
        <div class="card-background">
            <img class="card-bkimg" alt="" src="./ressource/bitmap.png">
        </div>
        <div class="useravatar">
            <img alt="" src="./ressource/a_c.jpg">
        </div>
        <div class="card-info"> <span class="card-title">Antoine - Developpeur</span>
        </div>
    </div>
    <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
        <div class="btn-group" role="group">
            <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab"><span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <div class="hidden-xs">Qui suis-je ?</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                <div class="hidden-xs">Mes langages</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <div class="hidden-xs">Mes outils</div>
            </button>
        </div>
    </div>
    <script>
    $(document).ready(function() {
$(".btn-pref .btn").click(function () {
    $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
    // $(".tab").addClass("active"); // instead of this do the below
    $(this).removeClass("btn-default").addClass("btn-primary");
});
});
    </script>
    <div class="well">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
          <h3>Etudiant de 22 ans à l'université Paris XIII</h3>
            <p>Passionné d'informatique depuis tout petit, j'aime créer de nouvelles choses </p>
        </div>
        <div class="tab-pane fade in" id="tab2">
          <h3>Langages Objet</h3>
            <div class="container">
                <div class="row">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                            <span class="sr-only">90%</span>
                        </div>
                        <span class="progress-type">PHP</span>
                        <span class="progress-completed">90%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                            <span class="sr-only">50%</span>
                        </div>
                        <span class="progress-type">JavaScript / jQuery</span>
                        <span class="progress-completed">50%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                            <span class="sr-only">70%</span>
                        </div>
                        <span class="progress-type">Java</span>
                        <span class="progress-completed">70%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            <span class="sr-only">40%</span>
                        </div>
                        <span class="progress-type">Python</span>
                        <span class="progress-completed">40%</span>
                    </div>
                </div>
            </div>
            <h3>Langages de Balisage</h3>
            <div class="container">
                <div class="row">
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                            <span class="sr-only">80% Complete</span>
                        </div>
                        <span class="progress-type">HTML / HTML5</span>
                        <span class="progress-completed">80%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                            <span class="sr-only">70% Complete (danger)</span>
                        </div>
                        <span class="progress-type">CSS / CSS3</span>
                        <span class="progress-completed">70%</span>
                    </div>
                </div>
            </div>
            <h3>Langages Procéduraux</h3>
            <div class="container">
                <div class="row">
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                            <span class="sr-only">60% Complete</span>
                        </div>
                        <span class="progress-type">PL/PLPG/SQL</span>
                        <span class="progress-completed">60%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                            <span class="sr-only">70%</span>
                        </div>
                        <span class="progress-type">C</span>
                        <span class="progress-completed">70%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            <span class="sr-only">40% Complete (info)</span>
                        </div>
                        <span class="progress-type">C++</span>
                        <span class="progress-completed">40%</span>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%">
                            <span class="sr-only">30%</span>
                        </div>
                        <span class="progress-type">Assembleur [MIPS]</span>
                        <span class="progress-completed">30%</span>
                    </div>
                </div>
            </div>
            <h3>Modélisation</h3>
            <ul>
                <li>Diagramme de classe</li>
                <li>Diagramme de séquence</li>
                <li>Diagramme de déploiement</li>
                <li>Diagramme d'utilisation</li>
            </ul>
        </div>
        <div class="tab-pane fade in" id="tab3">
          <h3 styke="margin-botton:50px;">Outils de développement <small>liste non exhaustive</small></h3>
          <div class="container">
            <div class="col-md-12">
              <div class="col-md-4">
                <img src="./ressource/wamp.png" alt="wamp" class="img-responsive"/>
              </div>
              <div class="col-md-8">
                <h4>WampServer</h4>
                <p>WampServer est une plate-forme de développement Web sous Windows
                  pour des applications Web dynamiques à l’aide du serveur Apache2,
                  du langage de scripts PHP et d’une base de données MySQL.
                  Il possède également PHPMyAdmin pour gérer plus facilement vos bases de données.
                </p>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="col-md-12">
              <div class="col-md-4">
                <img src="./ressource/atom.png" alt="atom" class="img-responsive"/>
              </div>
              <div class="col-md-8">
                <h4>Atom</h4>
                <p>
                  Editeur trés puissant, beaucoup de packages
                   disponibles, nottament pour eclaircir son code
                   et utiliser git sans effort. Grande capacité de modulation.
                </p>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="col-md-12">
              <div class="col-md-4">
                <img src="./ressource/merise.png" alt="merise" class="img-responsive"/>
              </div>
              <div class="col-md-8">
                <h4>JMerise</h4>
                <p>
                  Logiciel dédié à la modélisation des modèles conceptuels de données (MCD)
                  pour Merise : il permet la généralisation et la spécialisation des entités,
                  la création des relations et des cardinalités ainsi que la généralisation
                  des modéles logiques de données(MLD) et des script SQL..
                </p>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="col-md-12">
              <div class="col-md-4">
                <img src="./ressource/gantt.png" alt="gantt" class="img-responsive"/>
              </div>
              <div class="col-md-8">
                <h4>GanttProject</h4>
                <p>
                  Utile pour la gestion de projet, permet d'établir des diagrammes
                  de Gantt, WBS rapidement et efficacement. Il est écrit
                   en java, Open Source et libre de droits.
                </p>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="col-md-12">
              <div class="col-md-4">
                <img src="./ressource/modelio.png" alt="modelio" class="img-responsive"/>
              </div>
              <div class="col-md-8">
                <h4>Modelio</h4>
                <p>
                  Outils de modélisation UML : processus métiers, architecture,
                  analyse d'exigence, cas d'utilisation ... Il peut être facilement
                  lié à Eclipse
                </p>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="col-md-12">
              <div class="col-md-4">
                <img src="./ressource/eclipse.png" alt="eclipse" class="img-responsive"/>
              </div>
              <div class="col-md-8">
                <h4>Eclipse</h4>
                <p>
                  IDE utilisé pour mes developpements en Java & en C, il est capable de
                  gérer beaucoup de langages. Son outil de débug est trés puissant, et
                  l'on peut réaliser facilement avec JUnit des tests fonctionnels
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
