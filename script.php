    <!--  jQuery and Bootstrap core files    -->
    <script src="./js/jquery.js" type="text/javascript"></script>
	<script src="./js/jquery-ui.custom.min.js" type="text/javascript"></script>

	<script src="./bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Plugins -->
	<script src="./js/gsdk-checkbox.js"></script>
	<script src="./js/gsdk-morphing.js"></script>
	<script src="./js/gsdk-radio.js"></script>
	<script src="./js/gsdk-bootstrapswitch.js"></script>
	<script src="./js/bootstrap-select.js"></script>
	<script src="./js/bootstrap-datepicker.js"></script>
	<script src="./js/chartist.min.js"></script>
    <script src="./js/jquery.tagsinput.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!--  Get Shit Done Kit PRO Core javascript 	 -->
	<script src="./js/get-shit-done.js"></script>
    
    <!-- If you are using TypeKit.com uncomment this code otherwise you can delete it -->
    <!--
    <script src="https://use.typekit.net/[your kit code here].js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    -->

    <!-- If you have retina @2x images on your server which can be sent to iPhone/iPad/MacRetina, please uncomment the next line, otherwise you can delete it -->
	<!-- <script src="./js/retina.min.js"></script> -->